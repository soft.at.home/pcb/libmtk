/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <debug/sahtrace.h>

#include <pcb/core.h>
#include <pcb/pcb_client.h>

#include <mtk.h>
#include "mtk_priv.h"

static llist_t mtk_sos = {NULL, NULL};
static char* mtk_errorstring = NULL;
static mtk_so_t* mtk_current_so = NULL;

static int mtk_so_close_internal(mtk_so_t* so, bool doclose) {
    if(!so) {
        return 0;
    }
    if(mtk_synchronize()) {
        mtk_so_unregister(so);
    }
    mtk_module_t* module;
    for(module = (mtk_module_t*) llist_first(&so->modules); module; module = (mtk_module_t*) llist_iterator_next(&module->it)) {
        mtk_module_stop(module);
        module->info = NULL;
    }
    int ret = 0;
    if(so->handle) {
        SAH_TRACE_INFO("Unloading shared object %s ...", so->name);
        // old style pcb client
        // call pcb_client_cleanup, if the symbol is not found, ignore and continue
        void (* terminator)() =
            (void (*)())dlsym(so->handle, "pcb_client_cleanup");
        if(terminator) {
            SAH_TRACE_INFO("Calling pcb_client_cleanup");
            terminator();
        } else {
            // always call dlerror, otherwise the next dlsym will fail also
            dlerror();
        }
        if(doclose) {
            ret = dlclose(so->handle);
        }
        so->handle = NULL;
        SAH_TRACE_INFO("Unloaded shared object %s", so->name);
    }
    free(so->path);
    so->path = NULL;
    so->real = false;
    return ret;
}

void mtk_so_destroy_internal(mtk_so_t* so, bool doclose) {
    if(!so) {
        return;
    }
    while(!llist_isEmpty(&so->modules)) {
        mtk_module_destroy((mtk_module_t*) llist_first(&so->modules));
    }
    if(so->handle) {
        mtk_so_close_internal(so, doclose);
    }
    argument_valueClear(&so->args);
    free(so->name);
    llist_iterator_take(&so->it);
    free(so);
}


llist_t* mtk_so_list() {
    return &mtk_sos;
}

mtk_so_t* mtk_so_current() {
    return mtk_current_so;
}

mtk_so_t* mtk_so_find(const char* name) {
    mtk_so_t* so;
    for(so = (mtk_so_t*) llist_first(mtk_so_list()); so; so = (mtk_so_t*) llist_iterator_next(&so->it)) {
        if(!strcmp(so->name, name? name:"")) {
            return so;
        }
    }
    return NULL;
}

mtk_so_t* mtk_so_create(const char* name, bool enabled) {
    mtk_so_t* so = calloc(1, sizeof(mtk_so_t));
    if(!so) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    so->name = strdup(name);
    if(!so->name) {
        SAH_TRACE_ERROR("strdup(\"%s\") failed", name);
        goto error;
    }
    llist_append(mtk_so_list(), &so->it);
    so->enable = enabled;
    return so;
error:
    mtk_so_destroy(so);
    return NULL;
}

void mtk_so_destroy(mtk_so_t* so) {
    mtk_so_destroy_internal(so, true);
}

void mtk_so_register(mtk_so_t* so) {
    SAH_TRACE_IN();
    if(!so->real) {
        SAH_TRACE_OUT();
        return;
    }
    request_t* request = request_create_executeFunction(mtk_processPath(), "registerSharedObject",
                                                        request_common_path_key_notation | request_function_args_by_name);
    request_addCharParameter(request, "name", so->name);
    request_addCharParameter(request, "path", so->path);
    variant_map_t map0, map1;
    variant_list_t list0;
    variant_map_initialize(&map0);
    mtk_module_t* module;
    for(module = (mtk_module_t*) llist_first(&so->modules); module; module = (mtk_module_t*) llist_iterator_next(&module->it)) {
        if(!module->info) {
            continue;
        }
        variant_map_initialize(&map1);
        variant_list_initialize(&list0);
        mtk_function_info_t* info;
        for(info = module->info ? module->info->functions : NULL; info && info->name && info->cb; info++) {
            variant_list_addChar(&list0, info->name);
        }
        variant_map_addListMove(&map1, "functions", &list0);
        variant_list_cleanup(&list0);
        variant_map_addMapMove(&map0, module->name, &map1);
        variant_map_cleanup(&map1);
    }
    request_addMapParameter(request, "modules", &map0);
    variant_map_cleanup(&map0);
    mtk_request_push(request);
    SAH_TRACE_OUT();
}

void mtk_so_unregister(mtk_so_t* so) {
    if(!so->real) {
        return;
    }
    SAH_TRACE_INFO("calling unregisterSharedObject for %s.%s", mtk_processPath(), so->name);
    request_t* request = request_create_executeFunction(mtk_processPath(), "unregisterSharedObject",
                                                        request_common_path_key_notation | request_function_args_by_name);
    request_addCharParameter(request, "name", so->name);
    mtk_request_push(request);
}

mtk_so_t* mtk_so_open(const char* path, argument_value_list_t* args) {
    mtk_so_t* so;
    if(!path) {
        mtk_errorstring = strdup("Path is NULL");
        return NULL;
    }
    uint32_t length = strlen(path) + 12;
    char* buf = calloc(1, length);
    if(!buf) {
        SAH_TRACE_ERROR("malloc(%zd) failed", strlen(path) + 12);
        mtk_errorstring = strdup("Out of memory exception");
        return NULL;
    }
    strcpy(buf, path);
    char* buf0 = strrchr(buf, '/');
    if(buf0) {
        buf0++;
    } else {
        buf0 = buf;
    }
    char* buf1 = strchr(buf0, '.');
    if(buf1) {
        *buf1 = '\0';
    } else {
        buf1 = buf0 + strlen(buf0);
    }
    so = mtk_so_find(buf0);
    if(so && so->real) {
        *buf1++ = '_';
        uint32_t instance = 2;
        do {
            sprintf(buf1, "%u", instance++);
            so = mtk_so_find(buf0);
        } while (so && so->real);
    }
    if(!so) {
        so = mtk_so_find(buf0);
        if(!so) {
            so = mtk_so_create(buf0, mtk_isEnabled());
        }
    }
    free(buf);
    if(!so) {
        mtk_errorstring = strdup("Out of memory exception");
        return NULL;
    }
    so->path = strdup(path);
    argument_value_t* arg;
    while((arg = argument_valueTakeArgumentFirst(args))) {
        argument_valueAppend(&so->args, arg);
    }
    mtk_current_so = so;
    SAH_TRACE_INFO("Loading shared object %s(%s) ...", so->name, path);
    void* handle = dlopen(path, RTLD_NOW);
    if(!handle) {
        free(mtk_errorstring);
        char* e = dlerror();
        mtk_errorstring = strdup(e? e:"");
        SAH_TRACE_ERROR("Failed to load shared object %s(%s): %s", so->name, path, mtk_errorstring? mtk_errorstring:"");
        mtk_so_destroy(so);
        return NULL;
    } else {
        SAH_TRACE_INFO("Loaded shared object %s(%s)", so->name, path);
        so->real = true;
    }
    // old style pcb clients
    bool (* initializer)(pcb_t* pcb, int argc, char* argv[]) =
        (bool (*)(pcb_t* pcb, int argc, char* argv[]))dlsym(handle, "pcb_client_initialize");
    if(initializer) {
        client_config_t* cfg = pcb_clientConfig(mtk_getPcb());
        if(!initializer(mtk_getPcb(), cfg->client_argc, cfg->client_argv)) {
            SAH_TRACE_ERROR("pcb_client_initialize failed");
            mtk_so_destroy(so);
            dlclose(handle);
            return NULL;
        }
    } else {
        if(llist_isEmpty(&so->modules)) {
            free(mtk_errorstring);
            char* e = dlerror();
            mtk_errorstring = strdup(e? e:"");
            SAH_TRACE_ERROR("Failed to load shared object %s(%s): %s", so->name, path, mtk_errorstring? mtk_errorstring:"");
            mtk_so_destroy(so);
            dlclose(handle);
            return NULL;
        }
    }
    mtk_current_so = NULL;
    so->handle = handle;
    if(mtk_synchronize()) {
        SAH_TRACE_INFO("Sync with data model (%s) for shared object (%s)", mtk_processPath(), so->name);
        mtk_so_register(so);
    }
    return so;
}

const char* mtk_so_error() {
    return mtk_errorstring;
}

void* mtk_so_handle(mtk_so_t* so) {
    return so ? so->handle : NULL;
}

int mtk_so_close(mtk_so_t* so) {
    return mtk_so_close_internal(so, true);
}

void mtk_unload() {
    while(!llist_isEmpty(&mtk_sos)) {
        mtk_so_destroy_internal((mtk_so_t*) llist_first(&mtk_sos), getenv("MTK_NO_CLOSE")? false:true);
    }

    free(mtk_errorstring);
    mtk_errorstring = NULL;
}

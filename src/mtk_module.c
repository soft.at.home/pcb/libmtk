/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <debug/sahtrace.h>

#include <pcb/core.h>
#include <pcb/pcb_client.h>

#include <mtk.h>
#include "mtk_priv.h"

void mtk_getModuleArguments(argument_value_list_t* dst, argument_value_list_t* src, const char* name) {
    argument_value_t* arg;
    for(arg = argument_valueFirstArgument(src); arg; arg = argument_valueNextArgument(arg)) {
        char* argname = strdup(argument_valueName(arg));
        if(!argname) {
            SAH_TRACE_ERROR("strdup(\"%s\") failed", argument_valueName(arg));
            continue;
        }
        char* argnameptr = strchr(argname, '.');
        if(argnameptr) {
            *argnameptr++ = '\0';
            if(strcmp(argname, name)) {
                goto next;
            }
            argument_valueDestroy(argument_valueByName(dst, argname));
            argument_valueAdd(dst, argnameptr, argument_value(arg));
        } else {
            argument_valueDestroy(argument_valueByName(dst, argname));
            argument_valueAdd(dst, argname, argument_value(arg));
        }
next:
        free(argname);
    }
}

bool mtk_module_start(mtk_module_t* module) {
    if(module->running) {
        return false;
    }
    argument_value_list_t args = {NULL, NULL};
    mtk_getModuleArguments(&args, mtk_getArguments(), module->name);
    mtk_getModuleArguments(&args, &module->so->args, module->name);
    mtk_getModuleArguments(&args, &module->args, module->name);
    if(module->info && module->info->start) {
        SAH_TRACE_INFO("Starting module %s ...", module->name);
        module->running = module->info->start(&args);
        if(!module->running) {
            SAH_TRACE_WARNING("Failed to start module %s", module->name);
            mtk_callInformHandlers(module, mtk_event_module_start_failed);
        } else {
            SAH_TRACE_INFO("Started module %s", module->name);
            mtk_callInformHandlers(module, mtk_event_module_started);
        }
    }
    argument_valueClear(&args);
    return module->running;
}

void mtk_module_stop(mtk_module_t* module) {
    if(!module->running) {
        return;
    }
    if(module->info && module->info->stop) {
        SAH_TRACE_INFO("Stopping module %s ...", module->name);
        mtk_stop_defered(module->info->name);
        module->info->stop();
        mtk_callInformHandlers(module, mtk_event_module_stopped);
        SAH_TRACE_INFO("Stopped module %s", module->name);
    }
    module->running = false;
}

void mtk_module_destroy(mtk_module_t* module) {
    if(!module) {
        return;
    }
    mtk_module_stop(module);
    argument_valueClear(&module->args);
    llist_iterator_take(&module->it);
    free(module->name);
    free(module);
}

mtk_module_t* mtk_module_find(mtk_so_t* so, const char* name) {
    mtk_module_t* module;
    for(module = (mtk_module_t*) llist_first(&so->modules); module; module = (mtk_module_t*) llist_iterator_next(&module->it)) {
        if(!strcmp(module->name, name? name:"")) {
            return module;
        }
    }
    return NULL;
}

mtk_module_t* mtk_module_create(mtk_so_t* so, const char* name) {
    if(!name || !*name) {
        SAH_TRACE_ERROR("name is NULL");
        return NULL;
    }
    mtk_module_t* module = calloc(1, sizeof(mtk_module_t));
    if(!module) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    module->name = strdup(name);
    if(!module->name) {
        SAH_TRACE_ERROR("strdup(\"%s\") failed", name);
        goto error;
    }
    module->so = so;
    module->enable = so->enable;
    llist_append(&so->modules, &module->it);
    return module;
error:
    mtk_module_destroy(module);
    return NULL;
}

mtk_module_t* mtk_module_register_ex(const mtk_module_info_t* info, void* userdata) {
    mtk_so_t* so = mtk_so_current();
    if(!so) {
        so = mtk_so_find("anonymous");
        if(!so) {
            so = mtk_so_create("anonymous", mtk_isEnabled());
        }
    }
    if(!so) {
        return NULL;
    }
    mtk_module_t* module = mtk_module_find(so, info->name);
    if(!module) {
        module = mtk_module_create(so, info->name);
    }
    if(!module) {
        return NULL;
    }
    if(module->info) {
        SAH_TRACE_WARNING("Module with name %s already registered", info->name);
        return NULL;
    }
    module->info = info;
    if(!mtk_so_current() && mtk_synchronize()) {
        so->real = true;
        mtk_so_register(so);
    }
    module->userdata = userdata;
    if(module->enable) {
        mtk_module_start(module);
    }
    return module;
}

mtk_module_t* mtk_module_register(const mtk_module_info_t* info) {
    return mtk_module_register_ex(info, NULL);
}

void mtk_module_setUserdata(mtk_module_t* module, void* userdata) {
    if(!module) {
        return;
    }

    module->userdata = userdata;
}

void* mtk_module_getUserdata(mtk_module_t* module) {
    if(!module) {
        return NULL;
    }

    return module->userdata;
}

const char* mtk_module_getName(mtk_module_t* module) {
    if(!module) {
        return NULL;
    }

    return module->info->name;
}

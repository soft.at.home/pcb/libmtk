/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#include <pcb/pcb_client.h>

#include <mtk.h>
#include "mtk_priv.h"

typedef struct _mtk_deferred_function_data {
    llist_iterator_t it;
    pcb_timer_deferred_function_t fn;
    pcb_timer_t* timer;
    char* module;
    void* userdata;
} mtk_deferred_function_data_t;

static pcb_t* mtk_pcb = NULL;
static peer_info_t* mtk_sysbus = NULL;
static char* mtk_name = NULL;
static argument_value_list_t mtk_args = {NULL, NULL};
static char mtk_path[1024];
static bool mtk_sync = false;
static bool mtk_enabled = true;
static request_t* mtk_syncreq = NULL;
static llist_t mtk_informhandlers = { NULL, NULL };

static llist_t mtk_defered_functions = { NULL, NULL };

static void mtk_timer_call_deferred_function(pcb_timer_t* timer, void* userdata) {
    mtk_deferred_function_data_t* data = (mtk_deferred_function_data_t*) userdata;

    if(data) {
        llist_iterator_take(&data->it);
        if(data->fn) {
            data->fn(data->userdata);
        }
        free(data->module);
    }
    free(data);
    pcb_timer_destroy(timer);
}

static bool mtk_defaultReplyHandler(request_t* request, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) pcb; (void) from; (void) userdata;
    SAH_TRACE_INFO("request path = %s", request_path(request));
    reply_item_t* item;
    for(item = reply_firstItem(request_reply(request)); item; item = reply_nextItem(item)) {
        if(reply_item_type(item) == reply_type_error) {
            SAH_TRACE_ERROR("Function call error 0x%x (%s - %s)",
                            reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
            return false;
        }
    }
    return true;
}

static bool mtk_defaultDoneHandler(request_t* request, pcb_t* pcb, peer_info_t* from, void* userdata) {
    (void) pcb; (void) from; (void) userdata;
    request_destroy(request);
    return true;
}

static void mtk_defaultCancelHandler(request_t* request, void* userdata) {
    (void) userdata;
    request_destroy(request);
}

static bool mtk_request_invoke(request_t* request) {
    if(!pcb_sendRequest(mtk_getPcb(), mtk_getSysbus(), request)) {
        SAH_TRACE_ERROR("Failed to send request: 0x%x (%s)", pcb_error, error_string(pcb_error));
        return false;
    }

    if(!pcb_waitForReply(mtk_getPcb(), request, NULL)) {
        SAH_TRACE_ERROR("Failed to get reply: 0x%x (%s)", pcb_error, error_string(pcb_error));
        return false;
    }

    return mtk_defaultReplyHandler(request, NULL, NULL, NULL);
}

static const variant_t* mtk_request_returnValue(request_t* request) {
    reply_item_t* item;
    for(item = reply_firstItem(request_reply(request)); item; item = reply_nextItem(item)) {
        if(reply_item_type(item) == reply_type_function_return) {
            return reply_item_returnValue(item);
        }
    }
    return NULL;
}

static argument_value_list_t* mtk_request_returnArguments(request_t* request) {
    reply_item_t* item;
    for(item = reply_firstItem(request_reply(request)); item; item = reply_nextItem(item)) {
        if(reply_item_type(item) == reply_type_function_return) {
            return reply_item_returnArguments(item);
        }
    }
    return NULL;
}

static bool mtk_syncreqObject(object_t* object, void* userdata) {
    (void) userdata;
    mtk_so_t* so = NULL;
    mtk_module_t* module = NULL;
    bool enable = false;
    const char* processpath = NULL;
    char* pathptr = NULL;
    char* soname = NULL;
    char* modulename = NULL;

    char* path = object_pathChar(object, path_attr_key_notation);
    if(!path) {
        SAH_TRACE_ERROR("Could not get path of replied object");
        return true;
    }
    processpath = mtk_processPath();
    if(strncmp(path, processpath, strlen(processpath))) {
        goto leave;
    }
    pathptr = path + strlen(processpath);
    if(strncmp(pathptr, ".SharedObject.", sizeof(".SharedObject.") - 1)) {
        goto leave;
    }
    pathptr += sizeof(".SharedObject.") - 1;
    if(strncmp(pathptr, "Module", sizeof("Module") - 1)) {
        soname = pathptr;
        pathptr = strchr(pathptr, '.');
        if(!pathptr) {
            goto leave;
        }
        *pathptr++ = '\0';
    }
    if(strncmp(pathptr, "Module", sizeof("Module") - 1)) {
        goto leave;
    }
    pathptr += sizeof("Module") - 1;
    if(*pathptr == '.') {
        modulename = ++pathptr;
    }

    if(soname) {
        so = mtk_so_find(soname);
        if(!so) {
            so = mtk_so_create(soname, mtk_enabled);
        }
    }

    if(so && modulename) {
        module = mtk_module_find(so, modulename);
        if(!module) {
            module = mtk_module_create(so, modulename);
        }
    }

    if((soname && !so) || (modulename && !module)) {
        goto leave; // out of memory conditions
    }

    enable = object_parameterBoolValue(object, "Enable");
    if(module) {
        SAH_TRACE_INFO("Module %s", modulename);
        parameter_t* parameter;
        object_for_each_parameter(parameter, object) {
            const char* name = parameter_name(parameter);
            if(strcmp(name, "Enable") == 0) {
                continue;
            }
            argument_valueDestroy(argument_valueByName(&module->args, name));
            argument_valueAdd(&module->args, name, parameter_getValue(parameter));
        }

        if(module->enable && !enable) {
            SAH_TRACE_INFO("Stop module");
            mtk_module_stop(module);
        } else if(!module->enable && enable) {
            SAH_TRACE_INFO("Start module");
            mtk_module_start(module);
        }
        module->enable = enable;
    } else if(so) {
        SAH_TRACE_INFO("SharedObject %s", soname);
        so->enable = enable;
    } else {
        SAH_TRACE_INFO("MTK");
        mtk_enabled = enable;
    }
leave:
    free(path);
    return true;
}

static bool mtk_syncreqHandler(request_t* request, pcb_t* pcb, peer_info_t* from, void* userdata) {
    SAH_TRACE_IN();
    reply_item_t* item;
    for(item = reply_firstItem(request_reply(mtk_syncreq)); item; item = reply_nextItem(item)) {
        switch(reply_item_type(item)) {
        case reply_type_object: {
            object_t* object = reply_item_object(item);
            mtk_syncreqObject(object, NULL);
        }
        default:
            break;
        }
    }
    bool retval = mtk_defaultReplyHandler(request, pcb, from, userdata);
    SAH_TRACE_OUT();
    return retval;
}

static void mtk_createSyncRequest() {
    mtk_syncreq = request_create_getObject(mtk_processPath(), (uint32_t) -1,
                                           request_common_path_key_notation |
                                           request_getObject_parameters |
                                           request_notify_values_changed |
                                           request_notify_object_added |
                                           request_notify_object_deleted |
                                           request_no_object_caching |
                                           request_getObject_template_info);
    request_setReplyHandler(mtk_syncreq, mtk_syncreqHandler);
    if(!pcb_sendRequest(mtk_getPcb(), mtk_getSysbus(), mtk_syncreq)) {
        SAH_TRACE_ERROR("Failed to send request: 0x%x (%s)", pcb_error, error_string(pcb_error));
    }
}

static bool mtk_register(app_config_t* config, bool* sync) {
    request_t* request = request_create_executeFunction("Process", "register", request_function_args_by_name);
    request_addCharParameter(request, "name", config->name);

    request_addBoolParameter(request, "sync", *sync);

    // add root objects
    variant_list_t objects, objects_attr;
    variant_list_initialize(&objects);
    variant_list_initialize(&objects_attr);
    object_t* root = datamodel_root(pcb_datamodel(config->pcb));
    object_t* child = NULL;
    object_for_each_child(child, root) {
        SAH_TRACE_INFO("Adding root object [%s] with attr [%d]", object_name(child, path_attr_key_notation), object_attributes(child));
        variant_list_addChar(&objects, object_name(child, path_attr_key_notation));
        variant_list_addUInt32(&objects_attr, object_attributes(child));
    }
    request_addListParameter(request, "objects", &objects);
    variant_list_cleanup(&objects);
    request_addListParameter(request, "objectsattr", &objects_attr);
    variant_list_cleanup(&objects_attr);

    // add trace level
    request_addUInt32Parameter(request, "tracelevel", sahTraceLevel());

    // add trace zones
    variant_map_t tracezones;
    variant_map_initialize(&tracezones);
    sah_trace_zone_it* zone = sahTraceFirstZone();
    while(zone) {
        SAH_TRACE_INFO("Adding trace zone %s=%d", sahTraceZoneName(zone), sahTraceZoneLevel(zone));
        variant_map_addUInt32(&tracezones, sahTraceZoneName(zone), sahTraceZoneLevel(zone));
        zone = sahTraceNextZone(zone);
    }
    request_addMapParameter(request, "tracezones", &tracezones);
    variant_map_cleanup(&tracezones);

    // add namespace
    request_addCharParameter(request, "namespace", config->ns);

    // get result (busname + sync)
    bool ok = mtk_request_invoke(request);
    if(ok) {
        variant_map_t* traces = NULL;
        variant_map_iterator_t* it = NULL;
        mtk_name = variant_char(mtk_request_returnValue(request));
        argument_getBool(sync, mtk_request_returnArguments(request), request_function_args_by_name, "sync", false);
        argument_getMap(&traces, mtk_request_returnArguments(request), request_function_args_by_name, "traces", NULL);
        variant_map_for_each(it, traces) {
            if(strcmp(variant_map_iterator_key(it), "Enabled") == 0) {
                if(!variant_bool(variant_map_iterator_data(it))) {
                    sahTraceClose();
                    break;
                } else {
                    continue;
                }
            }
            if(strcmp(variant_map_iterator_key(it), "TraceLevel") == 0) {
                sahTraceSetLevel(variant_uint32(variant_map_iterator_data(it)));
                continue;
            }
            sahTraceAddZone(variant_uint32(variant_map_iterator_data(it)), variant_map_iterator_key(it));
        }
        variant_map_cleanup(traces);
        free(traces);
    } else {
        mtk_name = strdup(config->name);
    }
    request_destroy(request);
    config->busname = mtk_name;

    return true;
}

pcb_t* mtk_getPcb() {
    return mtk_pcb;
}

peer_info_t* mtk_getSysbus() {
    return mtk_sysbus;
}

const char* mtk_getName() {
    return mtk_name;
}

const char* mtk_processPath() {
    snprintf(mtk_path, sizeof(mtk_path), "Process.%s", mtk_name? mtk_name:"");
    return mtk_path;
}

bool mtk_synchronize() {
    return mtk_sync;
}

bool mtk_isEnabled() {
    return mtk_enabled;
}

argument_value_list_t* mtk_getArguments() {
    return &mtk_args;
}

void mtk_callInformHandlers(mtk_module_t* module, mtk_event_t event) {
    llist_iterator_t* it = NULL;
    mtk_inform_handler_t* data = NULL;
    llist_for_each(it, &mtk_informhandlers) {
        data = llist_item_data(it, mtk_inform_handler_t, it);
        if(data && data->fn) {
            data->fn(module, event);
        }
    }
}

void mtk_request_push(request_t* request) {
    request_setReplyHandler(request, mtk_defaultReplyHandler);
    request_setCancelHandler(request, mtk_defaultCancelHandler);
    request_setDoneHandler(request, mtk_defaultDoneHandler);
    if(!pcb_sendRequest(mtk_getPcb(), mtk_getSysbus(), request)) {
        SAH_TRACE_ERROR("Failed to send request: 0x%x (%s)", pcb_error, error_string(pcb_error));
        request_destroy(request);
    }
}

bool mtk_init(app_config_t* config, argument_value_list_t* args) {
    // if a name is already set registration was already done, skip
    if(mtk_name) {
        SAH_TRACE_NOTICE("Name was already set to %s, name %s not accepted", mtk_name, config->name);
        config->busname = mtk_name;
        return true;
    }

    mtk_pcb = config->pcb;
    mtk_sysbus = config->system_bus;

    argument_value_t* arg;
    while((arg = argument_valueTakeArgumentFirst(args))) {
        argument_valueAppend(&mtk_args, arg);
    }

    // if no connection to a bus is available, do not send the register and leave
    if(!mtk_sysbus) {
        SAH_TRACE_NOTICE("No connection to a bus is available, keep name %s", config->name);
        mtk_name = strdup(config->name);
        config->busname = mtk_name;
        return true;
    }

    // send register
    mtk_register(config, &config->sync);

    SAH_TRACE_INFO("Sync is %sabled", config->sync? "en":"dis");
    mtk_sync = config->sync;
    if(mtk_sync) {
        // first sync with data model
        pcb_client_get_objects(mtk_sysbus, mtk_processPath(), mtk_syncreqObject, NULL);
        // start motification request to keep out state up to date
        mtk_createSyncRequest();
    }

    return true;
}

bool mtk_add_informHandler(mtk_inform_handler_fn_t fn) {
    if(!fn) {
        return false;
    }

    llist_iterator_t* it = NULL;
    mtk_inform_handler_t* data = NULL;
    llist_for_each(it, &mtk_informhandlers) {
        data = llist_item_data(it, mtk_inform_handler_t, it);
        if(data) {
            if(data->fn == fn) {
                return true;
            }
        }
    }

    mtk_inform_handler_t* handler = calloc(1, sizeof(mtk_inform_handler_t));
    if(!handler) {
        return false;
    }
    handler->fn = fn;
    llist_append(&mtk_informhandlers, &handler->it);
    return true;
}

bool mtk_remove_informHandler(mtk_inform_handler_fn_t fn) {
    if(!fn) {
        return false;
    }

    llist_iterator_t* it = NULL;
    mtk_inform_handler_t* data = NULL;
    llist_for_each(it, &mtk_informhandlers) {
        data = llist_item_data(it, mtk_inform_handler_t, it);
        if(data) {
            if(data->fn == fn) {
                llist_iterator_take(it);
                free(data);
                break;
            }
        }
    }
    return true;
}

void mtk_start() {
    if(!mtk_sysbus) {
        SAH_TRACE_WARNING("Failed to connect to MTK data model");
    }

    // was already started
    if(mtk_syncreq) {
        return;
    }

    mtk_so_t* so = NULL;
    for(so = (mtk_so_t*) llist_first(mtk_so_list()); so; so = (mtk_so_t*) llist_iterator_next(&so->it)) {
        mtk_so_register(so);
    }

    mtk_createSyncRequest();

    if(mtk_syncreq) {
        mtk_sync = true;
    }
}

void mtk_stop() {
    if(mtk_sync) {
        request_destroy(mtk_syncreq);
        mtk_syncreq = NULL;
    }

    mtk_sync = false;
}

bool mtk_timer_defer(const char* module, pcb_timer_deferred_function_t fn, void* userdata, unsigned int time) {
    if(!fn) {
        return false;
    }

    pcb_timer_t* timer = pcb_timer_create();
    if(!timer) {
        return false;
    }

    mtk_deferred_function_data_t* data = (mtk_deferred_function_data_t*) calloc(1, sizeof(mtk_deferred_function_data_t));
    if(!data) {
        pcb_timer_destroy(timer);
        return false;
    }

    data->fn = fn;
    data->timer = timer;
    data->userdata = userdata;
    data->module = strdup(module);
    pcb_timer_setUserData(timer, data);
    pcb_timer_setHandler(timer, mtk_timer_call_deferred_function);

    llist_append(&mtk_defered_functions, &data->it);

    return pcb_timer_start(timer, time);
}

void mtk_stop_defered(const char* module) {
    if(!module || !(*module)) {
        return;
    }
    llist_iterator_t* it = llist_first(&mtk_defered_functions);
    llist_iterator_t* prefetch = NULL;
    mtk_deferred_function_data_t* data = NULL;
    while(it) {
        prefetch = llist_iterator_next(it);
        data = llist_item_data(it, mtk_deferred_function_data_t, it);
        if(data && data->module) {
            if(strcmp(data->module, module) == 0) {
                llist_iterator_take(it);
                pcb_timer_stop(data->timer);
                pcb_timer_destroy(data->timer);
                free(data->module);
                free(data);
            }
        }
        it = prefetch;
    }
}

__attribute__((destructor))
void mtk_cleanup() {
    mtk_unload();
    request_destroy(mtk_syncreq);
    mtk_syncreq = NULL;
    argument_valueClear(&mtk_args);
    free(mtk_name);
    mtk_name = NULL;
    mtk_pcb = NULL;
}

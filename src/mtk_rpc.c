/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#include <pcb/pcb_client.h>

#include <mtk.h>
#include "mtk_priv.h"

extern char** environ;

static void mtk_error_destroy(error_t* error) {
    if(!error) {
        return;
    }
    llist_iterator_take(&error->it);
    free(error->description);
    free(error->info);
    free(error);
}

static error_t* mtk_error_create(mtk_rpc_t* rpc, uint32_t code, const char* description, const char* info) {
    error_t* error = calloc(1, sizeof(error_t));
    if(!error) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    llist_append(&rpc->errors, &error->it);
    error->code = code;
    error->description = description? strdup(description):NULL;
    if(description && !error->description) {
        SAH_TRACE_ERROR("strdup(\"%s\") failed", description);
        goto error;
    }
    error->info = info? strdup(info):NULL;
    if(info && !error->info) {
        SAH_TRACE_ERROR("strdup(\"%s\") failed", info);
        goto error;
    }
    return error;
error:
    mtk_error_destroy(error);
    return NULL;
}

static void mtk_rpc_destroy(mtk_rpc_t* rpc) {
    if(!rpc) {
        return;
    }
    argument_valueClear(&rpc->args);
    variant_cleanup(&rpc->retval);
    while(!llist_isEmpty(&rpc->errors)) {
        mtk_error_destroy((error_t*) llist_first(&rpc->errors));
    }
    free(rpc->func);
    free(rpc);
}

static mtk_rpc_t* mtk_rpc_create(uint32_t id, const char* func) {
    mtk_rpc_t* rpc = calloc(1, sizeof(mtk_rpc_t));
    if(!rpc) {
        SAH_TRACE_ERROR("calloc() failed");
        goto error;
    }
    rpc->id = id;
    rpc->func = func ? strdup(func) : NULL;
    if(func && !rpc->func) {
        SAH_TRACE_ERROR("strdup(%s) failed", func);
        goto error;
    }
    variant_initialize(&rpc->retval, variant_type_unknown);
    return rpc;
error:
    mtk_rpc_destroy(rpc);
    return NULL;

}

argument_value_list_t* mtk_rpc_arguments(mtk_rpc_t* rpc) {
    return rpc ? &rpc->args : NULL;
}

uint32_t mtk_rpc_attributes(mtk_rpc_t* rpc) {
    return rpc ? rpc->attributes : 0;
}

variant_t* mtk_rpc_returnValue(mtk_rpc_t* rpc) {
    return rpc ? &rpc->retval : NULL;
}

void mtk_rpc_addError(mtk_rpc_t* rpc, uint32_t code, const char* description, const char* info) {
    if(rpc) {
        mtk_error_create(rpc, code, description, info);
    }
}

static void mtk_rpc_finish(mtk_rpc_t* rpc, uint32_t id, bool ok) {
    request_t* request = request_create_executeFunction(mtk_processPath(), "rpcFinish",
                                                        request_common_path_key_notation | request_function_args_by_name);
    request_addUInt32Parameter(request, "id", id);
    request_addBoolParameter(request, "ok", ok);
    if(!rpc) {
        goto skip;
    }
    if(variant_type(&rpc->retval) != variant_type_unknown) {
        request_addParameter(request, "retval", &rpc->retval);
    }
    if(!llist_isEmpty(&rpc->errors)) {
        variant_list_t errors;
        variant_list_initialize(&errors);
        error_t* error;
        for(error = (error_t*) llist_first(&rpc->errors); error; error = (error_t*) llist_iterator_next(&error->it)) {
            variant_map_t map;
            variant_map_initialize(&map);
            variant_map_addUInt32(&map, "code", error->code);
            if(error->description) {
                variant_map_addChar(&map, "description", error->description);
            }
            if(error->info) {
                variant_map_addChar(&map, "info", error->info);
            }
            variant_list_addMap(&errors, &map);
            variant_map_cleanup(&map);
        }
        request_addListParameter(request, "errors", &errors);
        variant_list_cleanup(&errors);
    }

    argument_value_t* arg;
    for(arg = argument_valueFirstArgument(&rpc->args); arg; arg = argument_valueNextArgument(arg)) {
        request_addParameter(request, argument_valueName(arg), argument_value(arg));
    }
skip:
    mtk_request_push(request);
}

static bool mtk_loadSharedObjectHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    char* sofile = NULL;
    if(!argument_getChar(&sofile, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "sofile", NULL)) {
        mtk_rpc_addError(rpc, pcb_error_function_argument_missing, "Mandatory argument missing", "sofile");
        goto leave;
    }
    if(!mtk_so_open(sofile, mtk_rpc_arguments(rpc))) {
        mtk_rpc_addError(rpc, pcb_error_system_error, mtk_so_error(), sofile);
        goto leave;
    }
    ok = true;
leave:
    free(sofile);
    return ok;
}

static bool mtk_unloadSharedObjectHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    char* name = NULL;
    mtk_so_t* so = NULL;

    if(!argument_getChar(&name, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "name", NULL)) {
        mtk_rpc_addError(rpc, pcb_error_function_argument_missing, "Mandatory argument missing", "name");
        goto leave;
    }
    so = mtk_so_find(name);
    if(!so) {
        mtk_rpc_addError(rpc, pcb_error_not_found, "No such shared object", name);
        goto leave;
    }
    mtk_so_close(so);
    ok = true;
leave:
    free(name);
    return ok;
}

static bool mtk_getEnvHandler(mtk_rpc_t* rpc) {
    char* name = NULL;
    argument_getChar(&name, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "name", NULL);
    if(name) {
        variant_setChar(mtk_rpc_returnValue(rpc), getenv(name));
    } else {
        variant_setType(mtk_rpc_returnValue(rpc), variant_type_map);
        variant_map_t* map = variant_da_map(mtk_rpc_returnValue(rpc));
        char** envn = environ;
        char* varianle_name = NULL;
        char* value = NULL;
        for(; *envn; envn++) {
            SAH_TRACE_INFO("[%s]", *envn);
            varianle_name = strdup(*envn);
            value = strchr(varianle_name, '=');
            if(!value) {
                free(varianle_name);
                continue;
            }
            value[0] = 0;
            value++;
            variant_map_addChar(map, varianle_name, value);
            free(varianle_name);
        }
    }
    free(name);
    return true;
}

static bool mtk_setEnvHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    char* name = NULL;
    char* value = NULL;
    if(!argument_getChar(&name, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "name", NULL)) {
        mtk_rpc_addError(rpc, pcb_error_function_argument_missing, "Mandatory argument missing", "name");
        goto leave;
    }
    if(!argument_getChar(&value, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "value", NULL)) {
        mtk_rpc_addError(rpc, pcb_error_function_argument_missing, "Mandatory argument missing", "value");
        goto leave;
    }
    setenv(name, value, 1);
    ok = true;
leave:
    free(name);
    free(value);
    return ok;
}

static bool mtk_startHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    mtk_getModuleArguments(&rpc->module->args, mtk_rpc_arguments(rpc), rpc->module->name);
    argument_valueClear(mtk_rpc_arguments(rpc));
    if(rpc->module->running) {
        mtk_rpc_addError(rpc, pcb_error_wrong_state, "Module is already running", rpc->module->name);
        goto leave;
    }
    rpc->module->enable = true;
    ok = mtk_module_start(rpc->module);
    if(!ok) {
        mtk_rpc_addError(rpc, pcb_error_unknown_system_error, "Failed to start module", rpc->module->name);
    }
leave:
    return ok;
}

static bool mtk_stopHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    if(!rpc->module->running) {
        mtk_rpc_addError(rpc, pcb_error_wrong_state, "Module is not running", rpc->module->name);
        goto leave;
    }
    rpc->module->enable = false;
    mtk_module_stop(rpc->module);
    ok = true;
leave:
    return ok;
}

static bool mtk_isRunningHandler(mtk_rpc_t* rpc) {
    variant_setBool(mtk_rpc_returnValue(rpc), rpc->module->running);
    return true;
}

static bool mtk_moduleDefaultHandler(mtk_rpc_t* rpc) {
    bool ok = false;
    mtk_function_info_t* info;
    if(!rpc->module->running) {
        mtk_rpc_addError(rpc, pcb_error_wrong_state, "Module is not running", rpc->module->name);
        goto leave;
    }
    for(info = rpc->module->info ? rpc->module->info->functions : NULL; info && info->name && info->cb; info++) {
        if(!strcmp(info->name, rpc->func? rpc->func:"")) {
            break;
        }
    }
    if(info && info->cb) {
        ok = info->cb(rpc);
    } else {
        mtk_rpc_addError(rpc, pcb_error_function_not_implemented, "Function is not implemented", rpc->func);
    }
leave:
    return ok;
}


static mtk_function_info_t mtk_funcs[mtk_func_level_count][5] = {
    { // mtk_func_level_process
        {
            .name = "loadSharedObject",
            .cb = mtk_loadSharedObjectHandler,
        },
        {
            .name = "unloadSharedObject",
            .cb = mtk_unloadSharedObjectHandler,
        },
        {
            .name = "getEnv",
            .cb = mtk_getEnvHandler,
        },
        {
            .name = "setEnv",
            .cb = mtk_setEnvHandler,
        },
        {
            .name = NULL,
            .cb = NULL,
        }
    }, { // mtk_func_level_so
        {
            .name = NULL,
            .cb = NULL,
        }
    }, { // mtk_func_level_module
        {
            .name = "start",
            .cb = mtk_startHandler,
        }, {
            .name = "stop",
            .cb = mtk_stopHandler,
        }, {
            .name = "isRunning",
            .cb = mtk_isRunningHandler,
        }, {
            .name = NULL,
            .cb = mtk_moduleDefaultHandler,
        }
    }
};

void mtk_funcHandler(notification_t* notification) {
    notification_parameter_t* parameter;
    mtk_rpc_t* rpc = NULL;
    bool ok = false;
    uint32_t id = 0;
    int level = 0;
    mtk_function_info_t* func = NULL;
    const char* notif_name = NULL;

    parameter = notification_getParameter(notification, "id");
    id = variant_uint32(notification_parameter_variant(parameter));
    notification_parameter_destroy(parameter);

    rpc = mtk_rpc_create(id, notification_name(notification));
    if(!rpc) {
        goto leave;
    }

    parameter = notification_getParameter(notification, "so");
    if(parameter) {
        rpc->so = mtk_so_find(variant_da_char(notification_parameter_variant(parameter)));
    }
    notification_parameter_destroy(parameter);

    parameter = notification_getParameter(notification, "module");
    if(rpc->so && parameter) {
        rpc->module = mtk_module_find(rpc->so, variant_da_char(notification_parameter_variant(parameter)));
    }
    notification_parameter_destroy(parameter);

    parameter = notification_getParameter(notification, "attributes");
    rpc->attributes = variant_uint32(notification_parameter_variant(parameter));
    notification_parameter_destroy(parameter);

    for(parameter = notification_firstParameter(notification); parameter; parameter = notification_nextParameter(parameter)) {
        argument_valueAdd(&rpc->args, notification_parameter_name(parameter), notification_parameter_variant(parameter));
    }

    level = (rpc->so? 1:0) + (rpc->module? 1:0);
    func = mtk_funcs[level];

    notif_name = notification_name(notification);
    while(func->name && strcmp(func->name, notif_name? notif_name:"")) {
        func++;
    }

    if(func->cb) {
        ok = func->cb(rpc);
    }

leave:
    mtk_rpc_finish(rpc, id, ok);
    mtk_rpc_destroy(rpc);
}

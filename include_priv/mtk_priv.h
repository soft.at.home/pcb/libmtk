/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef MTK_PRIV_H
#define MTK_PRIV_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>

#define PRIVATE __attribute__ ((visibility("hidden")))

struct _mtk_so {
    llist_iterator_t it;
    char* name;
    char* path;
    void* handle;
    argument_value_list_t args;
    llist_t modules;
    bool enable;
    bool real;
};

struct _mtk_module {
    llist_iterator_t it;
    char* name;
    argument_value_list_t args;
    mtk_so_t* so;
    const mtk_module_info_t* info;
    bool enable;
    bool running;
    void* userdata;
};

struct _mtk_rpc {
    uint32_t id;
    argument_value_list_t args;
    uint32_t attributes;
    variant_t retval;
    llist_t errors;
    mtk_so_t* so;
    mtk_module_t* module;
    char* func;
};

typedef struct _mtk_error {
    llist_iterator_t it;
    uint32_t code;
    char* description;
    char* info;
} error_t;

typedef struct _mtk_handler {
    llist_iterator_t it;
    mtk_inform_handler_fn_t fn;
} mtk_inform_handler_t;

typedef enum _mtk_func_level {
    mtk_func_level_process,
    mtk_func_level_so,
    mtk_func_level_module,
    mtk_func_level_count
} func_level_t;


const char PRIVATE* mtk_processPath();
const char PRIVATE* mtk_getName();
void PRIVATE mtk_request_push(request_t* request);
bool PRIVATE mtk_synchronize();
bool PRIVATE mtk_isEnabled();
argument_value_list_t PRIVATE* mtk_getArguments();
void PRIVATE mtk_callInformHandlers(mtk_module_t* module, mtk_event_t event);

llist_t PRIVATE* mtk_so_list();
mtk_so_t PRIVATE* mtk_so_current();
mtk_so_t PRIVATE* mtk_so_find(const char* name);
mtk_so_t PRIVATE* mtk_so_create(const char* name, bool enabled);
void PRIVATE mtk_so_destroy(mtk_so_t* so);
void PRIVATE mtk_so_register(mtk_so_t* so);
void PRIVATE mtk_so_unregister(mtk_so_t* so);

void PRIVATE mtk_getModuleArguments(argument_value_list_t* dst, argument_value_list_t* src, const char* name);
void PRIVATE mtk_module_destroy(mtk_module_t* module);
bool PRIVATE mtk_module_start(mtk_module_t* module);
void PRIVATE mtk_module_stop(mtk_module_t* module);
mtk_module_t PRIVATE* mtk_module_find(mtk_so_t* so, const char* name);
mtk_module_t PRIVATE* mtk_module_create(mtk_so_t* so, const char* name);

#ifdef __cplusplus
}
#endif

#endif // MTK_PRIV_H

/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <mtk.h>

#define ME "advanced"

typedef struct {
    uint32_t index;
    uint32_t count;
    llist_iterator_t it;
} tick_t;

static pcb_timer_t* advanced_timer;
static llist_t advanced_ticks = {NULL, NULL};
static uint32_t advanced_interval;
static uint32_t advanced_index;
static uint32_t advanced_limit;

static tick_t* advanced_tick_create(uint32_t index) {
    SAH_TRACEZ_INFO(ME, "Create tick [index=%u]", index);

    tick_t* tick = calloc(1, sizeof(tick_t));
    if(!tick) {
        SAH_TRACE_ERROR("calloc() failed");
        return NULL;
    }
    tick->index = index;
    llist_append(&advanced_ticks, &tick->it);
    return tick;
}

static tick_t* advanced_tick_find(uint32_t index) {
    llist_iterator_t* it;
    llist_for_each(it, &advanced_ticks) {
        tick_t* tick = llist_item_data(it, tick_t, it);
        if(tick->index == index) {
            return tick;
        }
    }
    return NULL;
}

static void advanced_tick_destroy(tick_t* tick) {
    SAH_TRACEZ_INFO(ME, "Destroy tick [index=%u]", tick->index);

    llist_iterator_take(&tick->it);
    free(tick);
}

static bool advanced_tick(mtk_rpc_t* rpc __attribute__((unused))) {
    tick_t* tick = advanced_tick_find(advanced_index);
    if(!tick) {
        tick = advanced_tick_create(advanced_index);
    }
    if(!tick) {
        goto leave;
    }

    tick->count++;

    SAH_TRACEZ_INFO(ME, "Hello index=%u count=%u", tick->index, tick->count);

leave:
    advanced_index++;
    if(advanced_index >= advanced_limit) {
        advanced_index = 0;
    }
    return true;
}

static void advanced_timer_handler(pcb_timer_t* timer __attribute__((unused)), void* userdata __attribute__((unused))) {
    advanced_tick(NULL);
}

static bool advanced_dump(mtk_rpc_t* rpc) {
    variant_map_t map;
    variant_map_initialize(&map);

    variant_list_t ticklist;
    variant_list_initialize(&ticklist);
    llist_iterator_t* it;
    llist_for_each(it, &advanced_ticks) {
        tick_t* tick = llist_item_data(it, tick_t, it);
        variant_map_t tickmap;
        variant_map_initialize(&tickmap);
        variant_map_addUInt32(&tickmap, "index", tick->index);
        variant_map_addUInt32(&tickmap, "count", tick->count);
        variant_list_addMapMove(&ticklist, &tickmap);
        variant_map_cleanup(&tickmap);
    }
    variant_map_addListMove(&map, "ticks", &ticklist);
    variant_list_cleanup(&ticklist);

    variant_map_addUInt32(&map, "interval", advanced_interval);
    variant_map_addUInt32(&map, "index", advanced_index);
    variant_map_addUInt32(&map, "limit", advanced_limit);
    variant_map_addUInt32(&map, "timer", pcb_timer_remainingTime(advanced_timer));

    variant_setMapMove(mtk_rpc_returnValue(rpc), &map);
    return true;
}

static void advanced_crash_actual() {
    int* dead = (int*) 0xdead;
    *dead = 1;
}

static void advanced_crash_caller() {
    advanced_crash_actual();
}

static void advanced_crash_yacaller() {
    advanced_crash_caller();
}

static bool advanced_crash(mtk_rpc_t* rpc __attribute__((unused))) {
    advanced_crash_yacaller();
    return true;
}

static void advanced_leak_actual() {
    int* leak = malloc(1000);
    *leak = 1;
}

static void advanced_leak_caller() {
    advanced_leak_actual();
}

static void advanced_leak_yacaller() {
    advanced_leak_caller();
}

static bool advanced_leak(mtk_rpc_t* rpc __attribute__((unused))) {
    advanced_leak_yacaller();
    advanced_limit = (uint32_t) -1;
    return true;
}

static bool advanced_set(mtk_rpc_t* rpc) {
    bool ok = true;
    argument_getUInt32(&advanced_index, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "index", advanced_index);
    argument_getUInt32(&advanced_limit, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "limit", advanced_limit);
    argument_getUInt32(&advanced_interval, mtk_rpc_arguments(rpc), mtk_rpc_attributes(rpc), "interval", advanced_interval);
    if(advanced_interval == 13) {
        SAH_TRACE_ERROR("Interval time of 13 seconds means bad luck. Fall back to default interval time 5");
        mtk_rpc_addError(rpc, pcb_error_invalid_value, "Invalid interval time", "13");
        advanced_interval = 5;
        ok = false;
    }
    pcb_timer_setInterval(advanced_timer, advanced_interval * 1000);
    pcb_timer_start(advanced_timer, 1);
    return ok;
}

static bool advanced_flush(mtk_rpc_t* rpc __attribute__((unused))) {
    while(!llist_isEmpty(&advanced_ticks)) {
        advanced_tick_destroy(llist_item_data(llist_first(&advanced_ticks), tick_t, it));
    }
    return true;
}

static bool advanced_start(argument_value_list_t* args) {
    SAH_TRACEZ_INFO(ME, "Starting ...");

    advanced_index = 0;

    advanced_interval = getenv("INTERVAL") ? atoi(getenv("INTERVAL")) : 5;
    argument_getUInt32(&advanced_interval, args, request_function_args_by_name, "interval", advanced_interval);
    if(advanced_interval == 13) {
        SAH_TRACE_ERROR("Interval time of 13 seconds means bad luck. Refusing to start");
        return false;
    }

    advanced_limit = getenv("LIMIT") ? atoi(getenv("LIMIT")) : 10;
    argument_getUInt32(&advanced_limit, args, request_function_args_by_name, "limit", advanced_limit);

    advanced_timer = pcb_timer_create();
    pcb_timer_setHandler(advanced_timer, advanced_timer_handler);
    pcb_timer_setInterval(advanced_timer, advanced_interval * 1000);
    pcb_timer_start(advanced_timer, 1);

    SAH_TRACEZ_INFO(ME, "Started");

    return true;
}

static void advanced_stop() {
    SAH_TRACEZ_INFO(ME, "Stopping ...");

    advanced_flush(NULL);

    pcb_timer_destroy(advanced_timer);

    SAH_TRACEZ_INFO(ME, "Stopped");
}

static mtk_function_info_t advanced_functions[] = {
    { .name = "dump", .cb = advanced_dump   },
    { .name = "crash", .cb = advanced_crash  },
    { .name = "leak", .cb = advanced_leak   },
    { .name = "tick", .cb = advanced_tick   },
    { .name = "set", .cb = advanced_set    },
    { .name = "flush", .cb = advanced_flush  },
    { .name = NULL },
};

static mtk_module_info_t advanced_info = {
    .name = ME,
    .start = advanced_start,
    .stop = advanced_stop,
    .functions = advanced_functions
};

__attribute__((constructor)) static void advanced_init() {
    mtk_module_register(&advanced_info);
}

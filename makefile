-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/libmtk
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libmtk

compile:
	$(MAKE) -C src all
	$(MAKE) -C pkgconfig all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C pkgconfig clean

install:
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).so $(D)/lib/$(COMPONENT).so.$(PV)
	ln -sfr $(D)/lib/$(COMPONENT).so.$(PV) $(D)/lib/$(COMPONENT).so
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)
	$(INSTALL) -D -p -m 0644 include/*.h $(D)$(INCLUDEDIR)/
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-mtk.pc $(PKG_CONFIG_LIBDIR)/mtk.pc
	$(INSTALL) -D -p -m 0644 pkgconfig/pkg-config-mtk-ssl.pc $(PKG_CONFIG_LIBDIR)/mtkssl.pc

.PHONY: compile clean install

